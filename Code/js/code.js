/**
 * Created by Stephanie on 21.05.2018.
 */
$(document).ready(function () {
    let epiparray=['white-shark','delphine', 'nemo', 'epip_info'];
    let mesoarray=['freier-taucher', 'taucher', 'big_plankton', 'eel', 'meso_info'];
    let batharray=['orca', 'kalamar', 'pottwal', 'titanic', 'bath_info']; //titanic
    let abysarray=['mr-crabs', 'abys_info'];
    let hadoarray=['worm', 'submarine', 'hado_info']; //borstenwurm, uboot, eventuell gebäude und berg
    let first=true;
    $(window).scroll(function (e) {
        if(first===true) {
            console.log("pls turn sound on");
            $("audio").prop('muted', false);
            first = false;
        }
        //epip
        for(let i =0; i<epiparray.length; i++){
            if($('.'+epiparray[i]).isOnScreen()){
                $('.'+epiparray[i]).addClass(epiparray[i]+'-animation');
                if($('.'+epiparray[i]).hasClass(epiparray[i]+'-out')){
                    $('.'+epiparray[i]).removeClass(epiparray[i]+'-out');
                }
            }
        }

        //meso
        for(let i =0; i<mesoarray.length; i++){
            if($('.'+mesoarray[i]).isOnScreen()){
                $('.'+mesoarray[i]).addClass(mesoarray[i]+'-animation');
                if($('.'+mesoarray[i]).hasClass(mesoarray[i]+'-out')){
                    $('.'+mesoarray[i]).removeClass(mesoarray[i]+'-out');
                }
                if(i==0){
                    for(let j=0; j<abysarray.length; j++){
                        $('.'+abysarray[j]).removeClass(abysarray[j]+'-animation');
                        $('.'+abysarray[j]).addClass(abysarray[j]+'-out');
                    }
                }
            }
        }
        //bath
        for(let i =0; i<batharray.length; i++){
            if($('.'+batharray[i]).isOnScreen()){
                $('.'+batharray[i]).addClass(batharray[i]+'-animation');
                if($('.'+batharray[i]).hasClass(batharray[i]+'-out')){
                    $('.'+batharray[i]).removeClass(batharray[i]+'-out');
                }
                if(i==0){
                    for(let j=0; j<epiparray.length; j++){
                        $('.'+epiparray[j]).removeClass(epiparray[j]+'-animation');
                        $('.'+epiparray[j]).addClass(epiparray[j]+'-out');
                    }
                    for(let j=0; j<epiparray.length; j++){
                        $('.'+hadoarray[j]).removeClass(hadoarray[j]+'-animation');
                        $('.'+hadoarray[j]).addClass(hadoarray[j]+'-out');
                    }
                }
            }
        }

        //abys
        for(let i =0; i<abysarray.length; i++){
            if($('.'+abysarray[i]).isOnScreen()){
                $('.'+abysarray[i]).addClass(abysarray[i]+'-animation');
                if($('.'+abysarray[i]).hasClass(abysarray[i]+'-out')){
                    $('.'+abysarray[i]).removeClass(abysarray[i]+'-out');
                }
                if(i==0){
                    for(let j=0; j<mesoarray.length; j++){
                        $('.'+mesoarray[j]).removeClass(mesoarray[j]+'-animation');
                        $('.'+mesoarray[j]).addClass(mesoarray[j]+'-out');
                    }
                }
            }
        }

        //hado
        for(let i =0; i<hadoarray.length; i++){
            if($('.'+hadoarray[i]).isOnScreen()){
                $('.'+hadoarray[i]).addClass(hadoarray[i]+'-animation');
                if($('.'+hadoarray[i]).hasClass(hadoarray[i]+'-out')){
                    $('.'+hadoarray[i]).removeClass(hadoarray[i]+'-out');
                }
                if(i==0){
                    for(let j=0; j<batharray.length; j++){
                        $('.'+batharray[j]).removeClass(batharray[j]+'-animation');
                        $('.'+batharray[j]).addClass(batharray[j]+'-out');
                    }
                }

                if(i==0){
                    for(let j=0; j<abysarray.length; j++){
                        $('.'+abysarray[j]).removeClass(abysarray[j]+'-animation');
                        $('.'+abysarray[j]).addClass(abysarray[j]+'-out');
                    }
                }
            }
        }
    })
});

$.fn.isOnScreen = function()
{
    var win = $(window);

    var viewport = {
        top : win.scrollTop()
    };
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};